<!-- Affichage de la timeline -->
	<?php
	    if (isset($_GET["lang"]) && $_GET["lang"] == "fr")
    	{
    		$lang = htmlspecialchars($_GET["lang"]);
    		$periods = array("La crise omniaque", "Blackwatch", "La Griffe");

    	}
    	else
    	{
    		$lang = "en";
    		$periods = array("The Omnic crisis", "Blackwatch", "Talon");
    	}
    	echo <<<"EOT"
    <head>
    	<meta charset="utf-8"/>
    	<link rel="icon" type="image/x-icon" href="../imgs/faviconow.ico"/>
    </head>
    <menu class="div_container">
      <h4> <a href="../controllers/controllerperiode-lore.php?lore=Omnic_crisis&lang=$lang"> $periods[0]</a> </h4>
      <h4> <a href="../controllers/controllerperiode-lore.php?lore=Blackwatch&lang=$lang"> $periods[1] </a> </h4>
      <h4> <a href="../controllers/controllerperiode-lore.php?lore=Talon&lang=$lang"> $periods[2] </a> </h4>
    </menu>
    <img src= "../imgs/$lang.png"id="lang_check"/>
    EOT;
	?>