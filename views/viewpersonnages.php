<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8"/>
    <title>
      Liste des personnages
    </title>
    <link rel="stylesheet" href="../css/personnages.css"/>
    <link rel="stylesheet" href="../css/globalstyle.css" />
  </head>
  <body>
    <?php include "../templates/timeline.php"; ?>
    <?php include "../templates/logo.php"; ?>
    <nav>
      <table>
        <thead>
          <tr>
            <th>Liste des personnages : </th>

          </tr>
        </thead>
        <tbody>

          <?php
                $descriptions = array();
                $skills = array_pad(array(),32,array());
                $id = -1;
                $old_category = NULL;
                foreach ($resultat["db"] as $key => $value) {
                  array_push($descriptions, $value['description']);
                  for ($i=1; $i <= 7 ; $i++) { 
                    array_push($skills[$key],array($value['nom_competence'.$i], $value['descr_competence'.$i]));
                  }
                  $category = $value['categorie'];
                  if ($category != $old_category) {
                    $old_category = $category;
                    echo "
                    <tr>
                      <th>$category</th>
                    </tr>\n";
                  }
                  $filename = mb_convert_encoding(strtolower($resultat["filenames"][$key]['nom']),"ASCII"); // on convertit en ascii de sorte à pouvoir détecter la présence du tréma (qui n'est pas correctement détecté par strrpos)
                  $pos_76 = strrpos($filename, ":");
                  $pos_o = strrpos($filename, "?");
                  // Il y aurait une méthode plus propre pour manipuler les strings de la base de données, mais il est plus simple de faire ça puisqu'on extrait déjà des cas spécifiques.
                  $name = $value['nom'];
                  
                  if ($pos_76)
                  {
                    $filename = "soldat-76";
                  }
                  else if ($pos_o)
                  {
                    $filename = "torbjorn";
                  }
                  if (!isset($_GET['lang']))
                  {
                    $_GET['lang'] = "en";
                  }
                  if (isset($_GET['fiche']) && $_GET['fiche'] == $filename)
                  {
                    $visibility = "visible";
                    $id = $value['id'];
                  }
                  else
                  {
                    $visibility = "hidden";
                    
                  }
                  echo "
                  <tr>
                    <td>
                      <a class=\"truc\" href=\"../controllers/controllerpersonnages.php?fiche=".$filename."&lang=".$lang."\">$name</a>
                      <img class='chibi' src=../imgs/".$filename."chibi.png
                   </td>
                  </tr>\n"; // Affichage des liens HTML
                  echo "<img class=\"imagesOW\" src=../imgs/".$filename."-concept.png alt=\"Overwatch\" style=\"visibility: $visibility;\"/>";
                  echo "<div class='pictures_caption'>".$value['maxime']."</div>";
                  echo "<img class=\"imgLucio\" src=../imgs/".$filename."-concept.png alt=\"Overwatch\"/>"; //Affichage des images
                }
                if (isset($_GET['fiche'])) {
                  echo "<div id='container'><h2 id='button1'> Description </h2> <h2 id='button2'> Skills </h2></div> <div id=\"text1\"></div>";
                  // mettre ici les instructions relatives aux fiches collectives des personnages_ow
                  // On terminera par un switch case pour les choses spécifiques, mais mieux vaut éviter un maximum
                }

            ?>
        </tbody>
      </table>
    </nav>
    <script type="text/javascript">let description = <?php echo json_encode($descriptions);?>; let id = <?php echo json_encode($id);?>; let skills = <?php echo json_encode($skills);?>; // non enregistré let skills =</script>
    <script src="../js/scripts.js"> </script>
    <script src="../js/global_scripts.js"> </script>
  </body>
</html>
