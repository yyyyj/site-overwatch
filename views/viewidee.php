<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../css/idee.css"/> <!-- Fichier spécifique -->
    <link rel="stylesheet" href="../css/globalstyle.css"> <!-- Fichier global à toutes les pages pour les paramètres par défaut -->
    <title>
      Overwatch lore
    </title>
  </head>
  <body>
    <header>
      <!-- On affiche l'image d'Overwatch dans le header, c'est-à dire en tête de page -->
    <?php echo"<a href=\"../controllers/controllerpersonnages.php?lang=$lang\">Personnages</a>"; ?>
    </header>
    <?php include "../templates/timeline.php"; ?> <!-- On inclut la timeline -->
    <article>
      <!-- On en capsule le tout dans article pour organiser le site et qu'il soit lisible. A Voir pour ne pas changer avec la balise menu -->

      <iframe width="560" height="315" src="https://www.youtube.com/embed/pyS3vmnWTyU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </article>
    <script src="../js/global_scripts.js"> </script>
  </body>
</html>
