<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8"/>
    <title>
      Etude du lore
    </title>
    <link rel="stylesheet" href="../css/globalstyle.css" />
    <link rel="stylesheet" href="../css/periode-lore.css"/>

  </head>
  <body>
    <?php include "../templates/timeline.php" ?>
    <?php include "../templates/logo.php"; ?>
    <?php 

        if (isset($_GET["lore"]))
        {
          if($_GET["lore"] == "Omnic_crisis")
          {
            echo "<h1>".$title.$periods[0]."</h1>";
            echo "<p>".$resultat['crise_omniaque']."</p>";
          }
          else if ($_GET["lore"] == "Blackwatch") 
          {
            echo "<h1>".$title.$periods[1]."</h1>";
            echo "<p>".$resultat['blackwatch']."</p>";
          }
          else if ($_GET["lore"] == "Talon") 
          {
            echo "<h1>".$title.$periods[2]."</h1>";
            echo "<p>".$resultat['griffe']."</p>";
          }
          else
          {
            echo "La page que vous avez demandé n'existe pas ! Peut-être avez-vous modifié votre URL ? N'hésitez pas à cliquer sur notre barre de recherche pour avoir d'autres informations sur le lore. ";
          }
        }
        else
        {
          echo "<h6> Que faites-vous ? Êtes-vous perdu sur le site ? </h6>";
        }
    ?>
  </body>
  <script src="../js/global_scripts.js"> </script>
</html>
