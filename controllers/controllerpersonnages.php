<?php
    require("../models/modelpersonnages.php");
    $data = new Datas();
    $lang = "en";
    if (isset($_GET["lang"]))
    {
    	$lang = htmlspecialchars($_GET["lang"]);
    }
    $resultat = $data -> getdatas_personnages($lang);
    require("../views/viewpersonnages.php");
