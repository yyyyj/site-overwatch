<?php

	require "../models/modelperiode-lore.php";
	$data = new Datas();
	if (isset($_GET["lang"]))
	{
		if ($_GET["lang"] == "fr")
		{
			$id = 0;
        	$title = "Période du lore : ";
        	$periods = array("La crise omniaque", "Blackwatch", "La Griffe");
		}
		else
		{
			$id = 1;
			$title = "Story period : ";
			$periods = array("The Omnic crisis", "Blackwatch", "Talon");
		}
	}
	else
	{
		$id = 1;
	}
	$resultat = $data -> getdatas_lore($id);

	require "../views/viewperiode-lore.php"; // Pour ne pas causer d'erreurs, le site de PHP préconise de ne pas fermer la balise