let lang = get_lang_with_url()[0]
let url = get_lang_with_url()[1];
document.getElementById('lang_check').addEventListener("click", function(){show_lang(lang,url)});
function show_lang(lang,url)
{


  if (lang == "fr")
  {
    location.assign(url+"=en");
    
  }
  else
  {
    location.assign(url+"=fr");

  }
}
function get_lang_with_url()
{
  let lang = "";
  let url = location.href.split("=");
  if (url.length <= 1)
  {
    url = url[0] +"?lang";
  }
  else
  {
    lang = url[url.length-1];
    url.pop()
    url = url.join('=');
  }
  return [lang,url];
}